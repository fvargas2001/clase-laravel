<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
	<body>
		<nav class="navbar navbar-expand-lg bg-body-tertiary">
			<div class="container-fluid">
				<a class="navbar-brand" href="#">First proyect</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarText">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
						<li class="nav-item">
						<a class="nav-link active" aria-current="page" href="#">Home</a>
						</li>
					</ul>
					<span class="navbar-text">
						<a href="{{ route('proyecto.create') }}"><button type="button" class="btn btn-outline-primary">Registered users</button></a>
					</span>
				</div>
			</div>
		</nav>
		<h1 style="margin: 50px 100px">Registered users </h1>
		<div class="container">
			<table class="table align-middle mb-0 bg-white" >
				<thead class="bg-light">
					<tr>
					<th>Name</th>
					<th>Address</th>
					<th>Update</th>
					<th>State</th>
					<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				@foreach ($proyectos as $proyecto)
					<tr>
						<td>
								<div class="ms-3">
									<p class="fw-bold mb-1">{{$proyecto->nombre_completo}}</p>
									<p class="text-muted mb-0">{{$proyecto->email}}</p>
								</div>
							</div>
						</td>
						<td>
							<p class="fw-normal mb-1">{{$proyecto->address}}</p>
							<p class="text-muted mb-0">{{$proyecto->city}}</p>
						</td>
						<td>{{$proyecto->updated_at}}</td>
						<td>
							<span class="badge text-bg-primary">Active</span>
						</td>
						<td>
							<a href="{{ route('proyecto.edit', $proyecto->id) }}"><button type="button" class="btn btn-primary btn-sm">Edit</button></a>
							<form action="{{ route('proyecto.destroy', $proyecto->id) }}" method="POST">
								@csrf
								@method('DELETE')
								<button type="button" class="btn btn-primary btn-sm" onclick ="return confirm">Eliminate</button>
							</form>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
		
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
	</body>
</html>